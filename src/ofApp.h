#pragma once

#include "ofMain.h"

class MyClass{
public:
	void set(int _i, float _f){
		i = _i; f = _f;
	};
	int i;
	float f;
};

class ofApp : public ofBaseApp{
public:

	int screenWidth, screenHeight;

	void setup();
	void update();
	void draw();
	void keyReleased(int key);
};
